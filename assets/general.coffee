#@prepros-prepend modernizr.js
#@prepros-prepend jquery.easings.min.js
#@prepros-prepend TweenMax.min.js
#@prepros-prepend slick.js
#@prepros-prepend retina.js
#@prepros-prepend jquery.fullPage.js

animatePageHeader = (el) ->
  tl = new TimelineLite()
  tl
  .from el.find('h2'), .6,
    opacity: 0
    scale: .9
    force3D: true
    ease: Back.easeOut
  .from el.find('.icon'), .6,
    opacity: 0
    force3D: true
    ease: Back.easeOut, '-=.6'
  .from el.find('p'), .6,
    scale: .9
    opacity: 0
    force3D: true
    ease: Back.easeOut, '-=.6'

$(document).ready ->

  pageScale = ->
    percentage = ($('.fp-tableCell').height() / 900).toFixed 2
    percentage = 1 if percentage > 1
    $('.fp-tableCell > .row').css
      transform: 'scale('+percentage+', '+percentage+')'

  $(window).load ->
    
    pageSlider = $('.b-page__carousel').slick
      speed: 200
      fade: true
      infinite: true
      arrows: false
      dots: true
      paddingTop: '120px'
      paddingBottom: '120px'
      autoplay: true
      autoplaySpeed: 3500
      onBeforeChange: (e, currentIndex, targetIndex) ->
        animateSlide3.restart() if targetIndex == 0
        animateSlide2.restart() if targetIndex == 1
        animateSlide1.restart() if targetIndex == 2
      onAfterChange: (e, index) ->

    $('.pages').css
      opacity: 1
      display: 'block'
    
    pageScale()

    $.fn.fullpage.moveTo 1
    animateSlide3.play()
  
  $(document).on 'click', '.b-main-slider__nav ul li a', (e) ->
    e.preventDefault()
    mainSlider.slickGoTo $(this).data 'slide-index'

  $(document).on 'click', '.b-main-slider__prev', (e) ->
    e.preventDefault()
    mainSlider.slickPrev()

  $(document).on 'click', '.b-main-slider__next', (e) ->
    e.preventDefault()
    mainSlider.slickNext()

  $(document).on 'click', '.b-nav__link', (e) ->
    e.preventDefault()
    return if $(this).find('a').hasClass 'is-active'
    $.fn.fullpage.moveTo $(this).find('a').data('page-index')

  $(document).on 'click', '.b-header__logo', (e) ->
    e.preventDefault()
    $.fn.fullpage.moveTo 1

  $(document).on 'click', '.b-header .b-button', (e) ->
    e.preventDefault()
    $.fn.fullpage.moveTo 6
 
  animateSlideArticle = new TimelineLite()
  animateSlideArticle
  .from '.b-main-slider__article h3', 1.5,
    opacity: 0 
    ease: Back.easeOut
  .from '.b-main-slider__article p', 1.5,
    opacity: 0
    ease: Back.easeOut, '-=1.5'

  animatePage1 = new TimelineLite()
  animatePage1
  .from $('.b-page#1 .b-button.is-filled'), .5,
    scale: .9
    opacity: 0
    force3D: true

  animatePage2 = new TimelineLite()
  animatePage2
  .add animatePageHeader($('.b-page#2 .b-page__header'))
  .from '.asset-8', .75,
    x: -100
    opacity: 0
  .from '.asset-9', .75,
    x: 100
    opacity: 0, '-=.75'
  .from '.b-feature-list.is-left', .75,
    x: -150
    opacity: 0, '-=.75'
  .from '.b-feature-list.is-right', .75,
    x: 150
    opacity: 0, '-=.75'
  .staggerFrom ['.asset-5', '.asset-6', '.asset-7'], .5,
    opacity: 0
    scale: .7
    ease: Back.easeOut, 0, '-=.1'
  .staggerFrom ['.asset-3', '.asset-4', '.asset-1', '.asset-2'], .5,
    scale: 0
    opacity: 0
    ease: Back.easeOut, 0
  .stop()

  animatePage3 = new TimelineLite()
  animatePage3
  .from $('.b-main-slider__nav'), .6,
    x: 150
    opacity: 0
  .from $('.b-main-slider__slide .col:nth-child(1)'), .6,
    x: -150
    opacity: 0, '-=.6'
  .from $('.b-main-slider__slide .col:nth-child(2)'), .6,
    x: 150
    opacity: 0, '-=.6'
  .from '.b-main-slider__prev', .4,
    scale: 0
    opacity: 0
  .from '.b-main-slider__next', .4,
    scale: 0
    opacity: 0, '-=.4'
  .stop()

  animatePage4 = new TimelineLite()
  animatePage4
  .add animatePageHeader($('.b-page#4 .b-page__header'))
  .staggerFrom '.step', .6,
    scale: .5
    opacity: 0
    ease: Back.easeOut, .25
  .stop()

  animatePage5 = new TimelineLite()
  animatePage5
  .add animatePageHeader($('.b-page#5 .b-page__header'))
  .staggerFrom $('.partners .partner'), .4,
    scale: .9
    opacity: 0
    ease: Back.easeOut, .1
  .stop()

  animatePage6 = new TimelineLite()
  animatePage6
  .add animatePageHeader($('.b-page#6 .b-page__header'))
  .from '.b-page#6 .b-button.is-filled', .5,
    scale: .9
    opacity: 0
    force3D: true, '-=.6'
  .from '.b-form__tip', .5,
    y: 50
    opacity: 0
  .stop()

  score = 
    score1: 0
    score2: 0

  animateSlide1 = new TimelineLite()
  animateSlide1
  .from '#score1', .5,
    x: -10
    scale: .4
    opacity: 0
    ease: Back.easeOut
  .from '.b-page__slide#1 .icon', .5,
    scale: .8
    opacity: 0
    force3D: true, '-=.5'
  .staggerFrom $('.b-page__slide#1 h2 > span'), .5,
    opacity: 0
    force3D: true, 0, '-=.5'

  animateSlide2 = new TimelineLite()
  animateSlide2
  .from '#score2', .5,
    x: -10
    scale: .4
    opacity: 0
    ease: Back.easeOut
  .from '.b-page__slide#2 .icon', .5,
    scale: .8
    opacity: 0
    force3D: true, '-=.5'
  .staggerFrom $('.b-page__slide#2 h2 > span'), .5,
    opacity: 0
    force3D: true, 0, '-=.5'
  .stop()

  animateSlide3 = new TimelineLite()
  animateSlide3
  .from '#score3', .5,
    x: -10
    scale: .4
    opacity: 0
    ease: Back.easeOut
  .from '.b-page__slide#3 .icon', .5,
    scale: .8
    opacity: 0
    force3D: true, '-=.5'
  .staggerFrom $('.b-page__slide#3 h2 > span'), .5,
    opacity: 0
    force3D: true, 0, '-=.5'
  .stop()

  animateMainSlide1 = new TimelineLite()
  animateMainSlide1
  .from '.asset-11', .75,
    opacity: .25

  animateMainSlide2 = new TimelineLite()
  animateMainSlide2
  .from '.asset-12', .75,
    opacity: .25

  animateMainSlide3 = new TimelineLite()
  animateMainSlide3
  .from '.asset-13', .75,
    opacity: .25

  animateMainSlide4 = new TimelineLite()
  animateMainSlide4
  .from '.asset-14', .5,
    x: -150
    opacity: 0
    force3D: true
  .from '.asset-16', .5,
    scale: 0
    opacity: 0
    rotation: -180
    force3D: true
    ease: Back.easeOut
  .from '.asset-15', .5,
    scale: 0
    opacity: 0
    force3D: true
    ease: Back.easeOut
  .stop()

  animateMainSlide5 = new TimelineLite()
  animateMainSlide5
  .staggerFrom '.asset-17, .asset-18', .5,
    left: -150
    opacity: 0
    force3D: true
    ease: Back.easeOut, .25
  .from '.asset-19', .5,
    scale: 0
    opacity: 0
    force3D: true
    ease: Back.easeOut


  pages = $('.pages').fullpage
    verticalCentered: true
    resize: false
    scrollingSpeed: 0
    touchSensitivity: 20
    sectionSelector: '.b-page'
    onLeave: (index, nextIndex, direction) ->

      anchor = $('.b-nav__anchor')

      animatePage1.restart().delay(.4) if nextIndex == 1
      animatePage2.restart().delay(.4) if nextIndex == 2
      animatePage3.restart().delay(.4) if nextIndex == 3
      animatePage4.restart().delay(.4) if nextIndex == 4
      animatePage5.restart().delay(.4) if nextIndex == 5
      animatePage6.restart().delay(.4) if nextIndex == 6

      mainSlider.slickGoTo 0 if index == 3

      if nextIndex in [1, 2, 3, 4, 5]
        $('.b-header .b-button').addClass('is-red')
      else
        $('.b-header .b-button').removeClass('is-red')
      
      $('.b-nav__link a').removeClass 'is-active'
      if nextIndex in [1, 2, 3, 4, 5]
        currentLink = $('.b-nav__link a').eq(nextIndex - 1)
        currentLink.addClass 'is-active'
        
        anchor
        .addClass 'is-active'
        .css
          width: currentLink.width() - 10
          left: currentLink.position().left + 5
      else
        anchor
        .removeClass 'is-active'

    afterResize: () ->
      pageScale()

  mainSlider = $('.b-main-slider__carousel').slick
    infinite: true
    speed: 0
    arrows: false
    draggable: false
    onBeforeChange: (e, currentIndex, targetIndex) ->
      nextSlide = $('.b-main-slider__slide#'+(targetIndex + 1))
      animateSlideArticle.restart()

      animateMainSlide1.restart() if targetIndex == 0
      animateMainSlide2.restart() if targetIndex == 1
      animateMainSlide3.restart() if targetIndex == 2
      animateMainSlide4.restart() if targetIndex == 3
      animateMainSlide5.restart() if targetIndex == 4

    onAfterChange: (e, index) ->
      $('.b-main-slider__nav ul li a').removeClass('is-active')
      $('.b-main-slider__nav ul li:nth-child('+(index+1)+') a').addClass('is-active')


